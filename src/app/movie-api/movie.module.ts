import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import { MovieRoutingModule } from './movie-routing.module';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { FilterPipe } from './services/filter.pipe';



@NgModule({
    declarations: [
        MovieListComponent,
        MovieDetailComponent,
        FilterPipe
    ],

    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MovieRoutingModule
    ],
    exports: [
        MovieListComponent
    ]
})
export class MovieModule {

}