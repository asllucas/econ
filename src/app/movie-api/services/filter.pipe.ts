import { Pipe, PipeTransform } from '@angular/core';
import { Movies } from '../models/movie';

@Pipe({
    name: 'filtermovie',
    pure: false
})
export class FilterPipe implements PipeTransform {

    transform(value: Movies[], filterBy: string): Movies[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((movie: Movies) =>
            movie.title.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}