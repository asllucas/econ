export class Movies {
    
    public id: number;
    public title: string;
    public original_language: string;
    public release_date: string;
    public poster_path: string;
    public overview: string;

}