import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MovieApi {

    headers = new HttpHeaders().set('Content-Type', 'application/json');

    baseUrl: string = 'https://api.themoviedb.org/3/movie/popular?';
    baseUrlById: string = 'https://api.themoviedb.org/3/movie/';
    baseUrlImage: string = 'https://api.themoviedb.org/3/movie/';
    api_key = "c09c9ff165dad7cc0a74f28709908f22";


    constructor(private http: HttpClient) { }

    public getListMovie() {
        return this.http.get(`${this.baseUrl}api_key=${this.api_key}&language=en-US&page=1`);
    }

    public getListMovieById(id): Observable<any> {
        const url = `${this.baseUrlById}${id}?api_key=${this.api_key}&language=en-US`;
        return this.http.get(url, { headers: this.headers }).pipe(
            map((res: Response) => {
                return res || {}
            }),
            catchError(this.errorMgmt)
        )
    }

    public getImageMovieById(id): Observable<any> {
        const url = `${this.baseUrlById}${id}/images?api_key=${this.api_key}&language=en-US`;
        return this.http.get(url, { headers: this.headers }).pipe(
            map((res: Response) => {
                return res || {}
            }),
            catchError(this.errorMgmt)
        )
    }


    errorMgmt(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }


}