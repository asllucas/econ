import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MovieApi } from '../../api/movie.api';
import * as moment from 'moment';


@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  public movies: any = [];
  public SearchMovie = '';
  moment: any = moment;

  constructor(private api: MovieApi, private _sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.getMovies()
  }

  public getMovies() {
    this.api.getListMovie().subscribe(movie => {
      this.movies = movie['results'];
    });
  }



}
