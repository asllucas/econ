import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { MovieApi } from '../../api/movie.api';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  public detailsForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private api: MovieApi,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.iniciarForm();

    const id = this.route.snapshot.paramMap.get('id');
    this.preencherForm(id);
  }
  
  private iniciarForm() {
    this.detailsForm = this.formBuilder.group({
      poster_path: [''],
      title: [''],
      original_language: [''],
      release_date: [''],
      overview: ['']
    })
  }

  private preencherForm(id) {
    this.api.getListMovieById(id).subscribe((movie) => {
      if (movie) {
        this.detailsForm.patchValue({
          poster_path: movie.overview,
          title: movie.title,
          original_language: movie.original_language,
          release_date: moment(movie.release_date, 'YYYY-MM-DD').format('DD/MM/YYYY'),
          overview: movie.overview
        })
      }
    })
  }

  public voltarListagem() {
    this.router.navigate(['/movies']);
  }

}
